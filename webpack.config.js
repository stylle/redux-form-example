module.exports = {
    devtool: "source-map",

    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"]
    },

    module: {
        rules: [
            { 
                test: /\.tsx?$/,
                loader: 'ts-loader'
            },
            {
                test: /\.css$/,
                use: [
                    {
                         loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true 
                        }
                    }
                ]
            },
        ]
    }
};