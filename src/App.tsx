import * as React from 'react';
import { Component } from 'react';
import { Provider } from 'react-redux';
import CssBaseline from '@material-ui/core/CssBaseline';
import store from './store';
import TestForm from './TestForm';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Provider store={store}>
        <div>
          <CssBaseline />
          <TestForm />
        </div>
      </Provider>
    );
  }
}

export default App;
