import * as React from 'react';
import { reduxForm, InjectedFormProps, Field, SubmissionError, FormSection } from 'redux-form';
import Selector from './shared/Selector';
import { saveUser } from './AC';
import MatTextField from './shared/MatTextField';
import MatRadioGroup from './shared/MatRadioGroup';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles, StyledComponentProps } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel/FormControlLabel';
import Radio from '@material-ui/core/Radio/Radio';
import Address from './Address';
import { connect } from 'react-redux';
import { required, maxLength, minLength } from './validators/validators';

const styles = theme => ({
  root: theme.mixins.gutters({
    width: 400,
    paddingTop: 16,
    paddingBottom: 16,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: theme.spacing.unit * 3
  }),

  buttons: {
    display: 'flex'
  },

  button: {
    marginLeft: 'auto'
  }
});

// normalizer
const upper = (value: string) => value && value.toUpperCase();

const submitForm = (user, dispatch) => {
  return new Promise((resolve, reject) => {
    dispatch(saveUser(user, resolve, reject));
  }).catch(error => {
    throw new SubmissionError({ ...error, _error: 'Submit failed!' });
  });
};

const firstNameValidators = [required, maxLength(10)];
const lastNameValidators = [required, maxLength(10), minLength(3)];

type TestFormProps = InjectedFormProps & StyledComponentProps & { currentUser };

const TestForm = (props: TestFormProps) => {
  const { handleSubmit, pristine, submitting, reset, classes } = props;

  return (
    <Paper className={classes.root} elevation={4}>
      <Typography variant="headline" component="h3">
        User Form
      </Typography>
      <form onSubmit={handleSubmit(submitForm)}>
        <Field name="id" label="Id" disabled component={MatTextField} />
        <Field
          name="firstName"
          label="First Name"
          validate={firstNameValidators}
          component={MatTextField}
        />
        <Field
          name="lastName"
          label="Last Name"
          normalize={upper}
          validate={lastNameValidators}
          component={MatTextField}
        />
        <Field name="gender" label="Gender" validate={required} component={MatRadioGroup}>
          <FormControlLabel value="female" control={<Radio />} label="Female" />
          <FormControlLabel value="male" control={<Radio />} label="Male" />
        </Field>
        <FormSection name="address">
          <Address />
        </FormSection>
        <div className={classes.buttons}>
          <Button disabled={pristine || submitting} className={classes.button} type="submit">
            Submit
          </Button>
          <Button type="button" disabled={pristine || submitting} onClick={reset}>
            Reset
          </Button>
        </div>
      </form>
    </Paper>
  );
};

export default connect((state: any) => {
  return {
    // initialValues: state.users.user
  };
})(
  reduxForm({
    form: 'testForm'
    // enableReinitialize: true
  })(withStyles(styles)(TestForm))
);
