import * as React from 'react';
import { Component } from 'react';
import Select, { Option } from 'react-select';

interface SelectorProps {
  label: string;
  options: Option<string>[];
  change: Function;
  input: any;
}

class Selector extends Component<SelectorProps> {
  constructor(props: any) {
    super(props);
  }

  handleOnChange = (selection: Option<string>) => {
    console.log('select on change: ', selection);
    const { name, onChange } = this.props.input;
    onChange(name, selection.value);
  };

  render() {
    const { label, options } = this.props;

    return (
      <div>
        <div>
          <div>{label}</div>
        </div>
        <Select options={options} onChange={this.handleOnChange} />
      </div>
    );
  }
}

export default Selector;
