import * as React from 'react';
import TextField from '@material-ui/core/TextField';
import withStyles from '@material-ui/core/styles/withStyles';
import FormControl from '@material-ui/core/FormControl/FormControl';

const styles = theme => ({
  control: {
    width: '100%',
    marginBottom: theme.spacing.unit
  }
});

const MatTextField = (props: any) => {
  const {
    input,
    label,
    meta: { touched, error },
    classes,
    ...custom
  } = props;

  const isError = !!(touched && error);

  return (
    <FormControl className={classes.control} component="fieldset">
      <TextField
        label={label}
        error={isError}
        helperText={isError && error}
        onChange={(event, value) => input.onChange(value)}
        {...input}
        {...custom}
      />
    </FormControl>
  );
};

export default withStyles(styles)(MatTextField);
