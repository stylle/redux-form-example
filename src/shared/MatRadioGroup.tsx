import * as React from 'react';
import RadioGroup from '@material-ui/core/RadioGroup/RadioGroup';
import FormControl from '@material-ui/core/FormControl/FormControl';
import FormLabel from '@material-ui/core/FormLabel/FormLabel';
import withStyles from '@material-ui/core/styles/withStyles';
import FormHelperText from '@material-ui/core/FormHelperText/FormHelperText';

const styles = theme => ({
  control: {
    marginTop: theme.spacing.unit
  },

  radioGroup: {
    display: 'flex',
    'flex-direction': 'row'
  }
});

const MatRadioGroup = (props: any) => {
  const {
    label,
    input,
    classes,
    meta: { error, touched },
    ...rest
  } = props;

  const isError = !!(touched && error);

  return (
    <FormControl error={isError} className={classes.control} component="fieldset">
      <FormLabel component="legend">{label}</FormLabel>
      <RadioGroup
        {...input}
        {...rest}
        className={classes.radioGroup}
        value={input.value}
        onChange={(event, value) => input.onChange(value)}
      />
      {isError && <FormHelperText>{error}</FormHelperText>}
    </FormControl>
  );
};

export default withStyles(styles)(MatRadioGroup);
