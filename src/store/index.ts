import { createStore, applyMiddleware, combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import thunk from 'redux-thunk';
import users from '../reducers/users';

const rootReducer = combineReducers({ users, form: formReducer });
const enhancer = applyMiddleware(thunk);
const store = createStore(rootReducer, {}, enhancer);

//dev only
window['store'] = store;

export default store;
