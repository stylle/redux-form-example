export const START = 'START';
export const SUCCESS = 'SUCCESS';
export const FAIL = 'FAIL';
export const SAVE_USER = 'SAVE_USER';

export function saveUser(user, resolve, reject) {
  return dispatch => {
    dispatch({
      type: SAVE_USER + START,
      payload: { user }
    });

    setTimeout(() => {
      if (user.firstName === 'Sam') {
        const error = {
          firstName: 'This name is unavailable'
        };

        dispatch({
          type: SAVE_USER + FAIL,
          payload: { error },
          error: true
        });

        reject(error);
        console.log('user saving failed');

        return;
      }

      const id = Math.random() + Date.now();
      dispatch({
        type: SAVE_USER + SUCCESS,
        payload: { id, user }
      });

      console.log('user saving success');
      resolve();
    }, 500);
  };
}
