import { SAVE_USER, SUCCESS, START } from '../AC';

const defaultState = {
  user: {
    firstName: 'Peter',
    lastName: 'Pan'
  }
};

export default (userState = defaultState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SAVE_USER + SUCCESS: {
      const { id, user } = payload;

      return {
        ...userState,
        user: {
          ...userState.user,
          ...user,
          id: id
        }
      };
    }
  }

  return userState;
};
