import * as React from 'react';
import { Component } from 'react';
import Field from 'redux-form/lib/Field';
import MatTextField from './shared/MatTextField';

class Address extends Component {
  render() {
    return (
      <div>
        <Field name="city" label="City" component={MatTextField} />
        <Field name="street" label="Street" component={MatTextField} />
      </div>
    );
  }
}

export default Address;
